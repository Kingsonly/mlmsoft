<?php

class admindashboard extends Controller {
	//public $data=array();
	public $loggedid;
	
	function __construct() {
		parent::__construct();
		Session::init();
		$this->logged = Session::get('loggedIn');
		$this->loggedType= Session::get('loggedType');
		$this->view->session_details=Session::get('details');
		//$loggedid = Session::get('loggedid');

		if (($this->logged == false)||($this->loggedType != 'admin')) {
			Session::destroy();
			header('location: ./adminlogin');
			exit;
		}
		
	
		$this->view->js = array('dashboard/js/default.js');
		
	}
	
	function index() 
	{	
		//$this->view->render('dashboard/adminindex',0,0,$logged=false);
	$message=$this->view->message='';

	$this->view->render('dashboard/adminindex', $noinclude=false, $message, $this->loggedType);
 
		//$this->data['id']=$this->loggedid;
	}

	function consultant() 
	{	
		//$message['getconsultants'] = $this->model->getconsultants();
$message=$this->view->message='';
	$this->view->render('dashboard/admincontent/consultant', $noinclude=false, $message);

	}
	
	function properties() 
	{	
$message=$this->view->message='';

		//$message['getproperties'] = $this->model->getproperties();
		$this->view->render('dashboard/admincontent/properties', $noinclude=false, $message);
		
	}
	
	function viewproperty() 
	{	
	$message=$this->view->message='';

		//$this->view->message=$message['viewproperty'] = $this->model->viewproperty($json=false);
		$this->view->render('dashboard/admincontent/viewproperty', $noinclude=false, $message);
		
	}
	
	function transactions() 
	{	
		//message['gettransactions'] = $this->model->gettransactions();
$message=$this->view->message='';

		$this->view->render('dashboard/admincontent/transactions', $noinclude=false, $message);
	}
	
	function commissions() 
	{	
	$message=$this->view->message='';

		//$message['gettransactions'] = $this->model->getcommissions();

		$this->view->render('dashboard/admincontent/commissions', $noinclude=false, $message);
	}
	
	
	
	function generation() 
	{	
	$message=$this->view->message='';

	//$message['consultant'] = $this->model->registerconsultant($json=false);
	$this->view->render('dashboard/admincontent/generation', $noinclude=false, $message);
		
	}



	function registerconsultant() 
	{	
	$message=$this->view->message='';

	$message['consultant'] = $this->model->registerconsultant($json=false);
	$this->view->render('dashboard/admincontent/registerconsultant', $noinclude=false, $message);
		
	}
	
	function registerclient() 
	{	
		$message=$this->view->message='';

	
		//$message['gettransactions'] = $this->model->getcommissions();
	$this->view->render('dashboard/admincontent/registerclient', $noinclude=false, $message);
		
	}
	
	function bulksms() 
	{	
		$message=$this->view->message='';

		//$message['gettransactions'] = $this->model->getcommissions();

		$this->view->render('dashboard/admincontent/bulksms', $noinclude=false, $message);

		
	}
	
	function logout()
	{
		
		Session::destroy();
		header('location:'.URL.'adminlogin');
		exit;
	}
	


}