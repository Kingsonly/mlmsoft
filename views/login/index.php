
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Multi Level</b> Marketing</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="login/run" method="post">
    <?php if(!empty($data['ReturnMessage']['msg'])){?>    
    <div class="alert alert-danger">
    <?php echo $data['ReturnMessage']['msg'];?> 
    </div>    
    <?php }?>    
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->
      <br/>
    <a href="<?php echo $dirlocation;?>forgotpassword">I forgot my password</a><br>
    <a href="<?php echo $dirlocation;?>register" class="text-center">Register as  a new member</a>

  </div>
  <!-- /.login-box-body -->
</div>