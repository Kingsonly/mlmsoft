<body>
  <div class="form">
      
      <ul class="tab-group" >
        <li class="tab active " style="width: 900px;"><a href="#signup">Sign Up</a></li>
        
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="registration/register_client" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" name="first_name" required autocomplete="off" value="" />
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input name="last_name" type="text"required autocomplete="off"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input name="email" type="email"required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input name="password" type="password"required autocomplete="off"/>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              
            <select name="gender"  required autocomplete="off" >
            <option value="0">
            <----Select Genser---->
            </option>
            <option value="1">
            MALE
            </option>
            <option  value="2">
            FEMALE
            </option>

            </select>
            </div>
        
            <div class="field-wrap">
              <label>
                Date Of Birth<span class="req">*</span>
              </label>
              <input name="dob" type="date"required autocomplete="off"/>
            </div>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label>
              State<span class="req">*</span>
              </label>
              <input name="state" type="text" required autocomplete="off" value="" />
            </div>
        
            <div class="field-wrap">
              <label>
                Phone<span class="req">*</span>
              </label>
              <input name="phone" type="tel"required autocomplete="off"/>
            </div>
          </div>
          <div class="field-wrap">
            <label>
              Country<span class="req">*</span>
            </label>
            <input name="country" type="text"required autocomplete="off"/>
          </div>

          <div class="field-wrap">
            <label>
              Acount Name<span class="req">*</span>
            </label>
            <input name="account_name" type="text"required autocomplete="off"/>
          </div>

          <div class="top-row">
            <div class="field-wrap">
              <label>
                Bank Name<span class="req">*</span>
              </label>
              <input name="bank_name"> type="text" required autocomplete="off" value="<?php echo !empty($id)? $id:' ';?>" />
            </div>
        
            <div class="field-wrap">
              <label>
                Account Number<span class="req">*</span>
              </label>
              <input name="account_number" type="text"required autocomplete="off"/>
            </div>
          </div>
          <input type="hidden" name="father" value="<?php echo !empty($id)? $id:' ';?>">
          <input type="hidden" name="gen" value="<?php echo !empty($gen)? $gen:' ';?>">


          
          <button type="submit" class="button button-block"/>Get Started</button>
          
          </form>

        </div>
        
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->