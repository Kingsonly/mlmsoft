<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="http://almsaeedstudio.com">MLM</a>.</strong> All rights
    reserved.
  </footer>
  
  </div>