<div id="fh5co-features">
<div class="container">
<div class="row">
<div class="col-md-12">

<div class="feature-left">
<span class="icon">
<i class="icon-profile-male"></i>
</span>
<div class="feature-copy">
<h3>FOCUS 1: EDUCATION</h3>
<h4>The "School Without Walls" Project</h4>
<p>
<ul>
<li>The School Without Walls project is the flagship project of the Life Builders Initiative and remains a core focus of the organisation. It is a response to a lack of access to education for children displaced by communal clashes and insurgency. </li>
<li>
As strong advocates for education as a tool for societal transformation, Life Builders Initiative educationally engages a group of internally displaced children to sustainably improve their lives and transform them into agents of change, both for their communities and the nation at large.
</li>
</ul>
</p>

</div>
</div>

</div>

<div class="col-md-6">
<div class="feature-left">
<span class="icon">
<i class="icon-happy"></i>
</span>
<div class="feature-copy">

<h3>Vision</h3>
<p>Provide access to formal education to the underserved; poor, less privileged and internally displaced persons in our community to ensure meaningful living</p>

</div>
</div>

</div>
<div class="col-md-6">
<div class="feature-left">
<span class="icon">
<i class="icon-wallet"></i>
</span>
<div class="feature-copy">
<h3>Mission</h3>
<p>Enable the integration of the poor, less privileged and internally displaced persons into the society via the means of transferable skills, knowledge and capacities that are developed through education</p>

</div>
</div>
</div>
</div>
</div>
</div>

<div id="fh5co-feature-product" class="fh5co-section-gray">
<div class="container">
<div class="row">


<div class="row row-bottom-padded-md">
<div class="col-md-12 text-center animate-box">

</div>
</div>
<div class="row">
<div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
<h3>The Journey So Far</h3>
<p></p>
</div>
</div>

<div class="row">
<div class="col-md-4">
<div class="feature-text">
<h3></h3>
<p>School Without Walls currently provides education to over 600 children With the support of our partners, we also provide quality meal daily to all the pupils .</p>
</div>
</div>
<div class="col-md-4">
<div class="feature-text">
<h3></h3>
<p>We are currently in two out of the five IDP camps in the FCT</p>
</div>
</div>
<div class="col-md-4">
<div class="feature-text">
<h3></h3>
<p>The School without Walls in Area 1 and the School without Walls in New Kuchingoro now have a School Clinic.</p>
</div>
</div>
</div>


</div>
</div>




<div id="fh5co-content-section" class="fh5co-section-gray">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
<h3>Meet Our Team</h3>
<p>What started as a family's intervention has grown to become a national phenomenon and a shining example of what humanitarian intervention in the Internally Displaced People (IDPs) saga should look like. Please meet the family who share their love for God and humanity, demonstrated by a selfless commitment in providing funding, vision, human and material provision to the underserved and that has given birth to the platform called 'Life Builders Initiative for Education and Societal Integration'. </p>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/mr_sanwo_olatunji_david.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>SANWO OLATUNJI-DAVID </span>
</div>
</div>
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/dr_mrs_folake_olatunji_david.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>DR. FOLAKE OLATUNJI-DAVID</span>
</div>
</div>
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/dr_olumuyiwa_ajibola_olowe.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>DR. OLUMUYIWA A. OLOWE</span>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-md-offset-4 text-center animate-box">
<br/>
<br/>
<a href="#" class="btn btn-primary btn-lg">Meet Our Team</a>
</div>
</div>
<!-- fh5co-content-section -->


<!-- END What we do -->


<div id="fh5co-blog-section" class="fh5co-section-gray">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
<h3>Recent From Blog</h3>
<p></p>
</div>
</div>
</div>
<div class="container">
<div class="row row-bottom-padded-md">
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_1.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_2.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="clearfix visible-sm-block"></div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_3.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="clearfix visible-md-block"></div>
</div>

<div class="row">
<div class="col-md-4 col-md-offset-4 text-center animate-box">
<a href="#" class="btn btn-primary btn-lg">Our Blog</a>
</div>
</div>

</div>
</div>
