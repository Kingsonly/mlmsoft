<div id="fh5co-features">
<div class="container">
<div class="row">
<div class="col-md-4">

<div class="feature-left">
<span class="icon">
<i class="icon-profile-male"></i>
</span>
<div class="feature-copy">
<h3>Become a volunteer</h3>
<p>
Life Builders Initiative for Education and Societal Integration is forged on a model of partnership and collaborations. We are very excited to join you to design cutting edge programmes and initiatives and then form alliances with you and your organization to meet the needs of the poor, the vulnerable and internally displaced persons.
</p>
<p><a href="#">Learn More</a></p>
</div>
</div>

</div>

<div class="col-md-4">
<div class="feature-left">
<span class="icon">
<i class="icon-happy"></i>
</span>
<div class="feature-copy">

<h3>Our Mission</h3>
<p><?php echo $namess1 ; ?>To change the pattern of sole dependence of the poor, refugees and the less privileged people on the government by reactivating and engaging their enormous endowment through educational programmes of discreet nature.To improve the lives of the economically disadvantaged.</p>
<p><a href="#">Learn More</a></p>
</div>
</div>

</div>
<div class="col-md-4">
<div class="feature-left">
<span class="icon">
<i class="icon-wallet"></i>
</span>
<div class="feature-copy">
<h3>Donation</h3>
<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
<p><a href="#">Learn More</a></p>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="fh5co-feature-product" class="fh5co-section-gray">
<div class="container">
<div class="row">


<div class="row row-bottom-padded-md">
<div class="col-md-12 text-center animate-box">

</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="feature-text">
<h3>Love</h3>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
</div>
</div>
<div class="col-md-4">
<div class="feature-text">
<h3>Compassion</h3>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
</div>
</div>
<div class="col-md-4">
<div class="feature-text">
<h3>Charity</h3>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
</div>
</div>
</div>


</div>
</div>


<div id="fh5co-portfolio">
<div class="container">

<div class="row">
<div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
<h3>Our Gallery</h3>
<p></p>
</div>
</div>


<div class="row row-bottom-padded-md">
<div class="col-md-12">
<ul id="fh5co-portfolio-list">

<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(public/images/SAM_0480850x638.jpg); ">
<a href="#" class="color-3">
<div class="case-studies-summary">
<span>Give Love</span>
<h2>Donation is caring</h2>
</div>
</a>
</li>

<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(public/images/DSC_8266.JPG); ">
<a href="#" class="color-4">
<div class="case-studies-summary">
<span>Give Love</span>
<h2>Donation is caring</h2>
</div>
</a>
</li>

<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(public/images/IMG_9793800x533.jpg); ">
<a href="#" class="color-5">
<div class="case-studies-summary">
<span>Give Love</span>
<h2>Donation is caring</h2>
</div>
</a>
</li>
<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(public/images/DSC_7950.JPG); ">
<a href="#" class="color-6">
<div class="case-studies-summary">
<span>Give Love</span>
<h2>Donation is caring</h2>
</div>
</a>
</li>
</ul>
</div>
</div>

<div class="row">
<div class="col-md-4 col-md-offset-4 text-center animate-box">
<a href="#" class="btn btn-primary btn-lg">See Gallery</a>
</div>
</div>


</div>
</div>



<div id="fh5co-content-section" class="fh5co-section-gray">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
<h3>Meet Our Team</h3>
<p>What started as a family's intervention has grown to become a national phenomenon and a shining example of what humanitarian intervention in the Internally Displaced People (IDPs) saga should look like. Please meet the family who share their love for God and humanity, demonstrated by a selfless commitment in providing funding, vision, human and material provision to the underserved and that has given birth to the platform called 'Life Builders Initiative for Education and Societal Integration'. </p>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/mr_sanwo_olatunji_david.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>SANWO OLATUNJI-DAVID </span>
</div>
</div>
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/dr_mrs_folake_olatunji_david.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>DR. FOLAKE OLATUNJI-DAVID</span>
</div>
</div>
<div class="col-md-4">
<div class="fh5co-testimonial text-center animate-box">
<figure>
<img src="public/images/dr_olumuyiwa_ajibola_olowe.jpg" alt="user">
</figure>
<blockquote>
<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
</blockquote>
<span>DR. OLUMUYIWA A. OLOWE</span>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-md-offset-4 text-center animate-box">
<br/>
<br/>
<a href="#" class="btn btn-primary btn-lg">Meet Our Team</a>
</div>
</div>
<!-- fh5co-content-section -->

<div id="fh5co-services-section">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
<h3>Our Thematic Areas. Support Us</h3>
<p></p>
</div>
</div>
</div>
<div class="container">
<div class="row text-center">
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Education And Social Intergration</h3>
<p>The School Without Walls project is the flagship project of the Life Builders Initiative and remains a core ..... </p>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Medical Services</h3>
<p>This focus entails provision of essential medical services to the internally displaced persons (IDPs) via the Mobile Clinic and School Clinic Projects. </p>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Food And Nutrition</h3>
<p>The IDP Food Bank Project has a mission to put in place a sustainable system for food assistance. As an accompanying project to the School Without Wall program</p>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Water Sanitation and Hygene (WASH)</h3>
<p>The IDP WASH Project entails drilling of water boreholes in IDP communities, provision of health promotional services, medical outreaches including post-traumatic training to the IDP people.</p>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Basic Shelter And Housing</h3>
<p>The project 'Erect a Tent' is a response to the plight of internally displaced persons who lack basic shelter and housing and consequently live under poor and precarious conditions </p>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="services animate-box">
<span><i class="icon-heart"></i></span>
<h3>Economic Empowerment</h3>
<p>This A skill acquisition program for IDP women and Men which is based in one of the IDP camps- Camp David- and accessible to all IDP communities.</p>
</div>
</div>
</div>
</div>
</div>

<!-- END What we do -->


<div id="fh5co-blog-section" class="fh5co-section-gray">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
<h3>Recent From Blog</h3>
<p></p>
</div>
</div>
</div>
<div class="container">
<div class="row row-bottom-padded-md">
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_1.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_2.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="clearfix visible-sm-block"></div>
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="fh5co-blog animate-box">
<a href="#"><img class="img-responsive" src="public/images/cover_bg_3.jpg" alt=""></a>
<div class="blog-text">
<div class="prod-title">
<h3><a href=""#>Medical Mission in Southern Kenya</a></h3>
<span class="posted_by">Sep. 15th</span>
<span class="comment"><a href="">21<i class="icon-bubble2"></i></a></span>
<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
<p><a href="#">Learn More...</a></p>
</div>
</div>
</div>
</div>
<div class="clearfix visible-md-block"></div>
</div>

<div class="row">
<div class="col-md-4 col-md-offset-4 text-center animate-box">
<a href="#" class="btn btn-primary btn-lg">Our Blog</a>
</div>
</div>

</div>
</div>
