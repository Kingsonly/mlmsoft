//var framework="frameworkmlm";
(function(){
  'use strict';
  var module = angular.module('mlmsoft', ['ngRoute','angularUtils.directives.dirPagination',]);

module.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MasterController',
		})
		.when('/registration', {
			templateUrl: 'registration.html',
			controller: 'registrationCtrl'
		})

		.otherwise({
			redirectTo: '/'
		});
});

    ///////////// THIS IS THE CONSULTANT CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE CONSULTANT PAGE
    /////////////////////////
  module.controller('consultantController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	$scope.dirlocation = datagrab.dirlocation;
    $scope.loader = true;
    $scope.currentPage = 1;
    $scope.pageSize = 10;

    return $http.get("http://"+datagrab.dirlocation+"api/consultant")
    .then(function(response) {
	$scope.consultant = response.data;
    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });

    }]);

    ///////////// THIS IS THE CLIENTS CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE CLIENTS PAGE
    /////////////////////////
  module.controller('clientsController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	$scope.dirlocation = datagrab.dirlocation;
    $scope.loader = true;
    $scope.currentPage = 1;
    $scope.pageSize = 10;

    return $http.get("http://"+datagrab.dirlocation+"api/clients")
    .then(function(response) {
	$scope.clients = response.data;
    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });

    }]);


    ///////////// THIS IS THE PROPERTY CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE PROPERTY PAGE
    /////////////////////////
  module.controller('propertyController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
    $scope.loader = true;
    $scope.currentPage = 1;
    $scope.pageSize = 10;

	$scope.dirlocation = datagrab.dirlocation;
	$scope.saveproperty = function(){
    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#saveproperty')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/properties?table=tbl_properties',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
         //alert(answer);
		 if(answer == 'false'){
			 $('#saveproperty').hide();
			 $('#result').show();
			  $('.alert').html('NEW PROPERTY DETAILS SAVED');
			  $('.loader').hide();
		 }
		 else{
			 $('#result').show();
			  $('.alert').html(answer);
			  $('.loader').hide();
		 }
         }
       });

    }


    return $http.get("http://"+datagrab.dirlocation+"api/properties?getdetails=1")
    .then(function(answer) {
	$scope.properties = angular.fromJson(answer.data.properties);
    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });

    }]);


    ///////////// THIS IS THE PROPERTY CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE PROPERTY PAGE
    /////////////////////////
  module.controller('viewpropertyController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	$scope.dirlocation = datagrab.dirlocation;
	var parameterValue = decodeURIComponent(window.location.search.match(/(\?|&)get\=([^&]*)/)[2]);


	$scope.editproperty = function(){
    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#editproperty')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/properties?table=tbl_properties&&edit='+parameterValue,
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
         //alert(answer);
		 if(answer == 'false'){
			 $('#editproperty').hide();
			 $('#result').show();
			  $('.alert').html('NEW PROPERTY DETAILS UPDATED');
			  $('.loader').hide();
		 }
		 else{
			 $('#result').show();
			  $('.alert').html(answer);
			  $('.loader').hide();
		 }
         }
       });

    }



    return $http.get("http://"+datagrab.dirlocation+"api/viewproperty?get="+parameterValue)
    .then(function(response) {
	$scope.property =  angular.fromJson(response.data.property);
	$scope.consultant =  angular.fromJson(response.data.consultant);

    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });

    }]);





    ///////////// THIS IS THE PROPERTY CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE PROPERTY PAGE
    /////////////////////////
  module.controller('transactionsController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
     $scope.loader = true;
    $scope.currentPage = 1;
    $scope.pageSize = 10;

    $scope.selectConsultant = function() {
    return $http.get("http://"+datagrab.dirlocation+"api/consultant")
    .then(function(response) {
    $scope.consultant = response.data;

    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });
    }


    $scope.selectProperty = function() {
    var value=$('#consultant_id').val();
    return $http.get("http://"+datagrab.dirlocation+"api/properties?getdetails=1")
    .then(function(response) {
    $scope.properties =  angular.fromJson(response.data.properties);

    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });
    }


    $scope.savetransaction = function(){

    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#savetransaction')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/transactions?table=tbl_transactions',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
         //alert(answer);
         if(answer == 'false'){
             $('#editproperty').hide();
             $('#result').show();
              $('.alert').html('NEW TRANSACTIONS DETAILS SAVED!');
              $('.loader').hide();
         }
         else{
             $('#result').show();
              $('.alert').html(answer);
              $('.loader').hide();
         }
         }
       });


    }


    return $http.get("http://"+datagrab.dirlocation+"api/transactions")
    .then(function(response) {
	$scope.transactions = response.data;
    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });



    }]);



    ///////////// THIS IS THE COMMISSIONS CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE COMMISSIONS PAGE
    /////////////////////////
  module.controller('commissionsController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
    return $http.get("http://"+datagrab.dirlocation+"api/commissions")
    .then(function(response) {
	$scope.commissions = response.data;
    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });

    }]);



    ///////////// THIS IS THE REGISTER CONSULTANT CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE CONSULTANT REGISTRATION PAGE
    /////////////////////////
  module.controller('registerConsultantController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	//var parameterValue='';

	//var parameterValue = decodeURIComponent(window.location.search.match(/(\?|&)getdetails\=([^&]*)/)[2]);

	$scope.register = function(){

    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#register')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/registerconsultant?table=tbl_consultant',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (response) {
         //alert(response);
		 if(response == 'false'){
			 $('#register').hide();
			 $('#result').show();

			 $('.alert').html('New consultant created successfully! Consultant should check his/her email for more update. THANKS');

			 $('.loader').hide();
		 }
		 else{

			 $('#result').show();
			  $('.alert').html(response);
			  $('.loader').hide();
		 }
         }
       });

    }
	}]);


    ///////////// THIS IS THE REGISTER CONSULTANT CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE CONSULTANT REGISTRATION PAGE
    /////////////////////////
  module.controller('registerClientController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {

	$scope.saveclient = function(){
    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#saveclient')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/registerclient?table=tbl_clients',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
         //alert(answer);
		 if(answer == 'false'){
			 $('#saveclient').hide();
			 $('#result').show();
			  $('.alert').html('ClIENT DETAILS AND PAYMENT TRANSACTIONS SAVED');
			  $('.loader').hide();
		 }
		 else{
			 $('#result').show();
			  $('.alert').html(answer);
			  $('.loader').hide();
		 }
         }
       });

    }

	return $http.get("http://"+datagrab.dirlocation+"api/registerclient?getdetails=1")
    .then(function(response) {
	$scope.consultant = angular.fromJson(response.data.consultant);
	$scope.properties = angular.fromJson(response.data.properties);

    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });


	}]);

	 ///////////// THIS IS THE BULKSMS CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE BULK SMS PAGE
    /////////////////////////


  module.controller('bulksmsController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	$scope.sendsms = function(){
    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#sendsms')[0]);
    //var fetch = JSON.parse(formData);

    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/bulksms',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
		 if(answer == 'false'){
			 $('#sendsms').hide();
			 $('#result').show();
			  $('.alert').html('SMS MESSAGE SENT!');
			  $('.loader').hide();
		 }
		 else{
			 $('#result').show();
			  $('.alert').html(answer);
			  $('.loader').hide();
		 }
         }
       });

    }


    }]);


///////////// THIS IS THE BULKSMS CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE BULK SMS PAGE
    /////////////////////////


  module.controller('sendEmailController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
    $scope.sendemail = function(){
    $('.loader').show();
    $('.result').hide();
    var formData = new FormData($('#sendemail')[0]);
    //var fetch = JSON.parse(formData);
    $.ajax({
         url: 'http://'+datagrab.dirlocation+'api/sendemail',
         type: 'POST',
         data: formData,
         async: false,
         cache: false,
         contentType: false,
         enctype: 'multipart/form-data',
         processData: false,
         success: function (answer) {
         if(answer == 'false'){
             $('#sendemail').hide();
             $('#result').show();
              $('.alert').html('Email message was sent successfully!');
              $('.loader').hide();
         }
         else{
             $('#result').show();
              $('.alert').html(answer);
              $('.loader').hide();
         }
         }
       });

    }


    }]);


    ///////////// THIS IS THE REGISTER CONSULTANT CONTROLLER///////
    ///// THIS CONTROLS EVERY ACTIVITY ON THE CONSULTANT REGISTRATION PAGE
    /////////////////////////
  module.controller('generationController', ['$scope','$http','infogathering', function($scope, $http, datagrab) {
	 $scope.dirlocation = datagrab.dirlocation;

	var parameterValue = decodeURIComponent(window.location.search.match(/(\?|&)getdetails\=([^&]*)/)[2]);
	$scope.params = parameterValue;


	$scope.getSplit = function(id, dataarray){
	var datasplit=dataarray.split(",");
	var getposition = datasplit.indexOf(id);
	//alert(getposition);
	return getposition;

	}


	return $http.get("http://"+datagrab.dirlocation+"api/generation?getdetails="+parameterValue)
    .then(function(response) {
	$scope.sons = angular.fromJson(response.data.sons);
	$scope.fathers = angular.fromJson(response.data.fathers);
	$scope.consultant = angular.fromJson(response.data.consultant);
	//alert($scope.fathers);

    },function errorCallback(response) {
    alert("Poor Internet Connection. Please Check your Wifi/Mobile Settings.");
    return response.status;
    });


	}]);



  module.controller ('LoginCtrl', function($scope, $location, $rootScope){
   $scope.person={username:'', password:''};

    $scope.login = function(){

    $('.result').hide();
    $.post("http://localhost/mlmsoft/api/applogin",
    {
        type: $scope.regType,
        loginusername: $scope.person.username,
        loginpassword: $scope.person.password
    },

    function(response, status){

    })
    .then(function(data){
       value = JSON.parse(data);
       alert(value.full_name);
       if(value.state!='1'){
        $('.result').addClass('alert-danger');
        $('.result').show();
        $('.result').html(data);

        }
        else{
        if($scope.regType=='1'){
        $rootScope.currentuser = data;

        window.location.href = '/#student'
        }
        else if($scope.regType=='2'){
         window.location.href = '/#dashboard'
        }
         //$location.path("/main");
        }

    })
    ;


    }



});

module.factory('infogathering', ['$http', function($http) {

    var dirlocation = 'portal.adloyaltybn.com/';
    //var dirlocation = 'localhost/mlmsoft/';
    return {dirlocation: dirlocation}

}])

})();
