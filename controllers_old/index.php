<?php

class Index extends Controller {

	function __construct() {
		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');
		if ($logged == false) {
			Session::destroy();
			header('location: ./login');
			exit;
		}
	}

	function index() {
                //$this->namess1='123499999999999999999999999999999999999999';
				echo '<script> location.replace("./dashboard"); </script>';

	}

	function details() {
		$this->view->render('index/index');
	}

}
